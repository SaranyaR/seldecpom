package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.Given;

public class HomePage extends Annotations{ 

	public HomePage() {
       PageFactory.initElements(driver, this);
	} 
	
	@Given("click on CRMSFA link")
	public MyHomePage clickCrmfsaLink() {
		WebElement eleCrmfsaLink = locateElement("link", "CRM/SFA");
		click(eleCrmfsaLink);  
		return new MyHomePage();

	}
	
	public LoginPage clickLogoutAtLoginPage() {
		WebElement eleTLogout = locateElement("link", "Logout");
		click(eleTLogout);  
		return new LoginPage();

	}

	
	
	

}







