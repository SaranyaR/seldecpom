package com.yalla.pages;

import org.openqa.selenium.WebElement;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.Given;

public class MyHomePage extends Annotations {
	
	
	public MyHomePage() {
		 
	}
	
	
	
	public MyLeads clickLeads()
	{
		WebElement eleLeads = locateElement("link", "Leads");
		click(eleLeads);
		return new MyLeads();
	}
	
	

}
