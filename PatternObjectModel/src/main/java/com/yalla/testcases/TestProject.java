package com.yalla.testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class TestProject {
	
	@Test
	public void testProject() {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe\\");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://acme-test.uipath.com/account/login");
		driver.manage().window().maximize();
		driver.findElementById("email").sendKeys("harinisoundararajan92@gmail.com");
		driver.findElementById("password").sendKeys("thangamailu2");
		driver.findElementById("buttonLogin").click();
		driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		WebElement ven = driver.findElementByXPath("//button[text()=' Vendors']");
		Actions act = new Actions(driver);
		act.moveToElement(ven).perform();
		driver.findElementByLinkText("Search for Vendor").click();
		driver.findElementById("vendorTaxID").sendKeys("RO212121");
		driver.findElementById("buttonSearch").click();
		WebElement vendorName = driver.findElementByXPath("//td[text()='Green Restaurant']");
		System.out.println("Vendor Name is"+vendorName);
		driver.close();
		
	}

}
