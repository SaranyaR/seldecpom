Feature: To create Lead in lepftaps application


@scenario1
Scenario: Creating a Lead
And enter the username as Demosalesmanager
And enter the password as crmsfa
And click on login button
And click on CRMSFA link
And click on Create Lead
And enter first name as sarani
And enter last name as Rajen
And enter the company name as CTS
When click on create Lead button
Then verify the lead is created


@scenario2
Scenario Outline: Creating a Lead with examples
And enter the username as <username>
And enter the password as <password>
And click on login button
And click on CRMSFA link
And click on Create Lead
And enter first name as <firstName>
And enter last name as <lastName>
And enter the company name as <companyName>
When click on create Lead button
Then verify the lead is created
Examples:
|username|password|firstName|lastName|companyName|
|demosalesmanager|crmsfa|saranya|Rajendran|Cognizant|

