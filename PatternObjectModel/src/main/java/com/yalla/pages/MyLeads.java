package com.yalla.pages;

import org.openqa.selenium.WebElement;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.Given;

public class MyLeads extends Annotations{

	
	public MyLeads(){
		
	}
	
	@Given("click on Create Lead")
	public CreateLead clickCreateLead() {
		WebElement eleLeads = locateElement("link", "Create Lead");
		click(eleLeads);
		return new CreateLead();
	}
	
	
}