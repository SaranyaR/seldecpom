package com.yalla.pages;

import org.openqa.selenium.WebElement;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class CreateLead extends Annotations {
	
	
	public CreateLead() {
		
	}
	
	@Given("enter the company name as (.*)")
	public CreateLead enterCompanyName(String companyName) {
		WebElement eleCompanyName = locateElement("id", "createLeadForm_companyName");
		clearAndType(eleCompanyName, companyName);
		return this;
		
	}
	
	
	@Given("enter first name as (.*)")
	public CreateLead enterFirstName(String FirstName) {
		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		clearAndType(eleFirstName, FirstName);
		return this;
	}
	
	@Given("enter last name as (.*)")
	public CreateLead enterLastName(String LastName) {
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		clearAndType(eleLastName, LastName);
		return this;
	}
	
	@When("click on create Lead button")
	public ViewLead clickCreateLead() {
		WebElement eleCreateLead = locateElement("class", "smallSubmit");
		click(eleCreateLead);  
		return new ViewLead();
	}

}
