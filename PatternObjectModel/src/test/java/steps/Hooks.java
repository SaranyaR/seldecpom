package steps;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.yalla.selenium.api.base.SeleniumBase;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends SeleniumBase{

	@Before
	public void beforeScenarioExecution(Scenario sc){
		
		System.out.println("The testcase name is: " + sc.getName());
		System.out.println("The example/scenario Id is: " + sc.getId());
		
		//before suite in reporter.java file
		reporter = new ExtentHtmlReporter("./reports/result.html");
		reporter.setAppendExisting(true); 
		extent   = new ExtentReports();
		extent.attachReporter(reporter);
		
		//before test in TC002 testcase
//		testcaseName = "TC002_CreateLead";
//		testcaseDec = "Click on create leaad and verify first name";
		author = "saranya";
		category = "smoke";
//		excelFileName = "TC002";
		
		//before class from reporter.java file
		test = extent.createTest(sc.getName(), sc.getId());
	    test.assignAuthor(author);
	    test.assignCategory(category);
	    
	    //before method in annotatons.java file
	    startApp("chrome", "http://leaftaps.com/opentaps");
	    
	    
	    
	    
		
	}
	
	@After
	public void afterScenarioExecution(Scenario sc){
		System.out.println("The status of the scenario either passed/failed/skipped/null(undefined from version 3.0 is : " + sc.getStatus());
		
		//after method in annotatons.java file
	    close();
	    
	    //after suite from reporter.java file
	    extent.flush();
	}
}
