package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
features = "src\\test\\java\\feature\\CreateLead.feature",//if we want to execute single feature file then the feature file name should be given, if incase we want to run all the testcase then give upto package name only like ("src\\test\\java\\feature") 
glue = {"com.yalla.pages", "steps"},
/*dryRun = true,//creates the step definition code for steps mentioned in feature file 
snippets = SnippetType.CAMELCASE*/
monochrome = true,
tags = "@scenario1"


)
public class RunTest {

}
