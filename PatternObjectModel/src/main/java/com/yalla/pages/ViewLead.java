package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.Then;

public class ViewLead extends Annotations {

	
	public ViewLead() {
		
	}
	
	public ViewLead verifyFirstName() {
		WebElement eleFirstName = locateElement("id", "viewLead_firstName_sp");
		verifyExactText(eleFirstName, "sar");
		return this;
	}
	
	//@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogout;
	@Then("verify the lead is created")
	public LoginPage clickLogout() {
		WebElement eleLogout = locateElement("link", "Logout");
		click(eleLogout);  
		return new LoginPage();

	}
	
	
}
